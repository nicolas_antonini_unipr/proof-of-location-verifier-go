package smartcontract

import (
	"fmt"
	"time"

	"context"
	cpt "crypto"
	"crypto/ed25519"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/base64"
	"errors"
	"strconv"
	"strings"

	"github.com/algorand/go-algorand-sdk/client/kmd"
	"github.com/algorand/go-algorand-sdk/client/v2/algod"
	"github.com/algorand/go-algorand-sdk/client/v2/common/models"
	"github.com/algorand/go-algorand-sdk/crypto"
	"github.com/algorand/go-algorand-sdk/future"
	"github.com/algorand/go-algorand-sdk/types"
	"org.nicolasantonini/verifier/internal/certificates"
)

var AlgodAddress string
var KmdAddress string
var AlgodToken string
var KmdToken string

var AppId int

var AlgodClient *algod.Client
var KmdClient *kmd.Client
var Account crypto.Account

func waitForConfirmation(txID string, client *algod.Client, timeout uint64) (models.PendingTransactionInfoResponse, error) {
	pt := new(models.PendingTransactionInfoResponse)
	if client == nil || txID == "" || timeout < 0 {
		var msg = errors.New("bad arguments for waitForConfirmation")
		return *pt, msg

	}

	status, err := client.Status().Do(context.Background())
	if err != nil {
		var msg = errors.New(strings.Join([]string{"error getting algod status: "}, err.Error()))
		return *pt, msg
	}
	startRound := status.LastRound + 1
	currentRound := startRound

	for currentRound < (startRound + timeout) {

		*pt, _, err = client.PendingTransactionInformation(txID).Do(context.Background())
		if err != nil {
			var msg = errors.New(strings.Join([]string{"error getting pending transaction: "}, err.Error()))
			return *pt, msg
		}
		if pt.ConfirmedRound > 0 {
			return *pt, nil
		}
		if pt.PoolError != "" {
			var msg = errors.New("there was a pool error, then the transaction has been rejected")
			return *pt, msg
		}
		status, err = client.StatusAfterBlock(currentRound).Do(context.Background())
		if err != nil {
			return *pt, errors.New("error getting status")
		}
		currentRound++
	}
	msg := errors.New("tx not found in round range")
	return *pt, msg
}

func Setup(privateKeyString, algodAddress, algodToken, kmdAddress, kmdToken string, appId int) (bool, error) {
	AlgodAddress = algodAddress
	KmdAddress = kmdAddress
	AlgodToken = algodToken
	KmdToken = kmdToken
	AppId = appId

	privateKeyBytes, err := base64.RawStdEncoding.DecodeString(privateKeyString)
	if err != nil {
		return false, errors.New("unable to decode base64 encoded private key")
	}

	privateKey := ed25519.PrivateKey(privateKeyBytes)
	Account, err = crypto.AccountFromPrivateKey(privateKey)
	if err != nil {
		return false, errors.New("unable to get account from Private Key")
	}

	fmt.Printf("Account address: %s\n", Account.Address.String())

	AlgodClient, err = algod.MakeClient(algodAddress, algodToken)
	if err != nil {
		return false, errors.New("enable to get an algod client")
	}

	accountInfo, err := AlgodClient.AccountInformation(Account.Address.String()).Do(context.Background())
	if err != nil {
		return false, errors.New("error getting account info")
	}
	fmt.Printf("Account balance: %d microAlgos\n", accountInfo.Amount)

	txParams, err := AlgodClient.SuggestedParams().Do(context.Background())
	if err != nil {
		fmt.Printf("Error getting suggested tx params: %s\n", err)
		return false, errors.New("error getting suggested tx params")
	}

	verifierPubKey, _ := base64.RawStdEncoding.DecodeString(certificates.PublicRSAKey)

	appArgs := [][]byte{[]byte("setup"), verifierPubKey}
	txn, err := future.MakeApplicationNoOpTx(uint64(appId), appArgs, nil, nil, nil, txParams, Account.Address, nil, types.Digest{}, [32]byte{}, types.Address{})
	if err != nil {
		return false, errors.New("failed to create setup transaction")
	}

	txID, signedTxn, err := crypto.SignTransaction(Account.PrivateKey, txn)
	if err != nil {
		return false, errors.New("failed to sign transaction")
	}

	_, err = AlgodClient.SendRawTransaction(signedTxn).Do(context.Background())
	if err != nil {
		return false, errors.New("failed to send transaction")
	}

	_, err = waitForConfirmation(txID, AlgodClient, 4)
	if err != nil {
		return false, errors.New("failed to wait for transaction confirmation")
	}

	return true, nil
}

func ParseRsaPrivateKeyFromPemStr(privPEM string) (*rsa.PrivateKey, error) {
	pemData, err := base64.StdEncoding.DecodeString(privPEM)
	if err != nil {
		return nil, err
	}
	priv, err := x509.ParsePKCS8PrivateKey(pemData)
	if err != nil {
		return nil, err
	}

	return priv.(*rsa.PrivateKey), nil
}

func PostProofOfLocation(pseudonym string, latitude float64, longitude float64, timestamp time.Time) error {
	txParams, err := AlgodClient.SuggestedParams().Do(context.Background())
	if err != nil {
		return fmt.Errorf("error getting suggested tx params")
	}

	latitudeString := strconv.FormatFloat(latitude, 'E', -1, 64)
	longitudeString := strconv.FormatFloat(longitude, 'E', -1, 64)
	timestampString := time.UnixDate

	verifierPrivateKeyBase64String := certificates.PrivateRSAKey
	verifierPrivateKey, _ := ParseRsaPrivateKeyFromPemStr(verifierPrivateKeyBase64String)
	checksum := sha256.Sum256([]byte(latitudeString + longitudeString + timestampString))

	signature, err := rsa.SignPKCS1v15(rand.Reader, verifierPrivateKey, cpt.SHA256, checksum[:])
	if err != nil {
		return fmt.Errorf("failed to sign the hash of proof of location: %s", err)
	}

	appArgs := [][]byte{[]byte("post_pol"), []byte(pseudonym), []byte(latitudeString), []byte(longitudeString), []byte(timestampString), signature}
	txn, err := future.MakeApplicationNoOpTx(17, appArgs, nil, nil, nil, txParams, Account.Address, nil, types.Digest{}, [32]byte{}, types.Address{})
	if err != nil {
		return fmt.Errorf("failed to create transaction: %s", err)
	}

	txID, signedTxn, err := crypto.SignTransaction(Account.PrivateKey, txn)
	if err != nil {
		return fmt.Errorf("failed to sign transaction: %s", err)
	}
	fmt.Printf("Signed txid: %s\n", txID)

	_, err = AlgodClient.SendRawTransaction(signedTxn).Do(context.Background())
	if err != nil {
		return fmt.Errorf("failed to send transaction: %s", err)
	}

	_, err = waitForConfirmation(txID, AlgodClient, 4)
	if err != nil {
		return fmt.Errorf("frror waiting for confirmation")
	}

	return nil
}
