package publickey

import (
	routing "github.com/go-ozzo/ozzo-routing/v2"
	"org.nicolasantonini/verifier/internal/certificates"
	"org.nicolasantonini/verifier/internal/log"
)

func RegisterHandlers(r *routing.RouteGroup, authHandler routing.Handler, logger log.Logger) {
	res := resource{logger}

	r.Get("/public-key", res.get)
}

type resource struct {
	logger log.Logger
}

func (r resource) get(c *routing.Context) error {
	return c.Write(certificates.PublicRSAKey)
}
