package accesslog

import (
	"net/http"
	"time"

	routing "github.com/go-ozzo/ozzo-routing/v2"
	"github.com/go-ozzo/ozzo-routing/v2/access"
	"org.nicolasantonini/verifier/internal/log"
)

func Handler(logger log.Logger) routing.Handler {
	return func(c *routing.Context) error {
		start := time.Now()
		responseWriter := &access.LogResponseWriter{ResponseWriter: c.Response, Status: http.StatusOK}

		c.Response = responseWriter

		ctx := c.Request.Context()
		ctx = log.WithRequest(ctx, c.Request)
		c.Request = c.Request.WithContext(ctx)

		err := c.Next()

		logger.With(ctx,
			"duration",
			time.Since(start).Milliseconds(),
			"status", responseWriter.Status).
			Infof("%s %s %s %d %d",
				c.Request.Method,
				c.Request.URL.Path,
				c.Request.Proto,
				responseWriter.Status,
				responseWriter.BytesWritten)

		return err
	}
}
