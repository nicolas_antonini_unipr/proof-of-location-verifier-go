package entity

import "time"

type PolRequest struct {
	ID           string    `json:"id"`
	PolId        string    `json:"pol_id"`
	ProverId     string    `json:"prover_id"`
	WitnessesIds []string  `json:"witnesses_ids"`
	CreatedAt    time.Time `json:"created_at"`
	ExpiresAt    time.Time `json:"expires_at"`
	Latitude     float64   `json:"latitude"`
	Longitude    float64   `json:"longitude"`
}
