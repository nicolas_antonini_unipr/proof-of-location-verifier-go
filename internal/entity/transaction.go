package entity

type Transactions struct {
	ID        string
	PolID     string
	ProverID  string
	WitnessID string
}
