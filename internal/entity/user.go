package entity

import "time"

type Users struct {
	ID         string
	Name       string
	PubID1     string
	PubID2     string
	Password   string
	LastUpdate time.Time
	PublicKey  string
}

// GetID returns the user ID.
func (u Users) GetID() string {
	return u.ID
}

// GetName returns the user name.
func (u Users) GetName() string {
	return u.Name
}

func (u Users) GetPubID1() string {
	return u.PubID1
}

func (u Users) GetPubID2() string {
	return u.PubID2
}

func (u Users) GetLastUpdate() time.Time {
	return u.LastUpdate
}

func (u Users) GetPublicKey() string {
	return u.PublicKey
}
