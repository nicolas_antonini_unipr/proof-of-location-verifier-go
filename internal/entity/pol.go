package entity

import "time"

type ProofOfLocation struct {
	ID        string    `json:"id"`
	PolId     string    `json:"pol_id"`
	ProverID  string    `json:"uid"`
	Timestamp time.Time `json:"ts"`
	Latitude  float64   `json:"latitude"`
	Longitude float64   `json:"longitude"`
}
