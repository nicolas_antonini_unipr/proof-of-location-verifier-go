package entity

import "github.com/google/uuid"

// GenerateID generates a unique ID that can be used as an identifier for an entity.
func GenerateID() string {
	return uuid.New().String()
}

//Returns a 16 char (128 bit) ID
func Generate16byteID() string {
	gen1 := uuid.New().String()
	part1 := gen1[len(gen1)-8:]
	gen2 := uuid.New().String()
	part2 := gen2[len(gen2)-8:]

	return part1 + part2
}

//Returns a 20 char (160 bit) ID
func Generate20byteID() string {
	gen1 := uuid.New().String()
	part1 := gen1[len(gen1)-10:]
	gen2 := uuid.New().String()
	part2 := gen2[len(gen2)-10:]

	return part1 + part2
}
