package entity

import (
	"time"
)

type CheckIn struct {
	ID           string    `json:"id"`
	UserID       string    `json:"user-id"`
	Latitude     float64   `json:"latitude"`
	Longitude    float64   `json:"longitude"`
	RegisteredAt time.Time `json:"registered-at"`
	ExpiresAt    time.Time `json:"expires-at"`
}
