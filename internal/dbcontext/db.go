package dbcontext

import (
	"context"

	dbx "github.com/go-ozzo/ozzo-dbx"
	routing "github.com/go-ozzo/ozzo-routing/v2"
)

type DB struct {
	db *dbx.DB
}

type TransactionFunc func(ctx context.Context, f func(ctx context.Context) error) error

type contextKey int

const (
	txKey contextKey = iota
)

func New(db *dbx.DB) *DB {
	return &DB{db}
}

func (db *DB) DB() *dbx.DB {
	return db.db
}

func (db *DB) With(ctx context.Context) dbx.Builder {
	if tx, ok := ctx.Value(txKey).(*dbx.Tx); ok {
		return tx
	}
	return db.db.WithContext(ctx)
}

func (db *DB) Transactional(ctx context.Context, f func(ctx context.Context) error) error {
	return db.db.TransactionalContext(ctx, nil, func(tx *dbx.Tx) error {
		return f(context.WithValue(ctx, txKey, tx))
	})
}

func (db *DB) TransactionHandler() routing.Handler {
	return func(c *routing.Context) error {
		return db.db.TransactionalContext(c.Request.Context(), nil, func(tx *dbx.Tx) error {
			ctx := context.WithValue(c.Request.Context(), txKey, tx)
			c.Request = c.Request.WithContext(ctx)
			return c.Next()
		})
	}
}
