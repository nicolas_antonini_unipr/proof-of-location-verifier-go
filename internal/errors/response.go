package errors

import (
	"net/http"
	"sort"

	validation "github.com/go-ozzo/ozzo-validation/v4"
)

type ErrorResponse struct {
	Status  int         `json:"status"`
	Message string      `json:"message"`
	Details interface{} `json:"details,omitempty"`
}

func (e ErrorResponse) Error() string {
	return e.Message
}

func (e ErrorResponse) StatusCode() int {
	return e.Status
}

func InternalServerError(msg string) ErrorResponse {
	if msg == "" {
		msg = "We encountered an error while processing your request."
	}
	return ErrorResponse{
		Status:  http.StatusInternalServerError,
		Message: msg,
	}
}

func NotFound(msg string) ErrorResponse {
	if msg == "" {
		msg = "The requested resource was not found."
	}
	return ErrorResponse{
		Status:  http.StatusNotFound,
		Message: msg,
	}
}

func Conflict(msg string) ErrorResponse {
	if msg == "" {
		msg = "The resource cannot be accessed."
	}
	return ErrorResponse{
		Status:  http.StatusConflict,
		Message: msg,
	}
}

func Unauthorized(msg string) ErrorResponse {
	if msg == "" {
		msg = "You are not authenticated to perform the requested action."
	}
	return ErrorResponse{
		Status:  http.StatusUnauthorized,
		Message: msg,
	}
}

func Forbidden(msg string) ErrorResponse {
	if msg == "" {
		msg = "You are not authorized to perform the requested action."
	}
	return ErrorResponse{
		Status:  http.StatusForbidden,
		Message: msg,
	}
}

func BadRequest(msg string) ErrorResponse {
	if msg == "" {
		msg = "Your request is in a bad format."
	}
	return ErrorResponse{
		Status:  http.StatusBadRequest,
		Message: msg,
	}
}

func Gone(msg string) ErrorResponse {
	if msg == "" {
		msg = "Access to the target resource is no longer available."
	}
	return ErrorResponse{
		Status:  http.StatusGone,
		Message: msg,
	}
}

func UnprocessableEntity(msg string) ErrorResponse {
	if msg == "" {
		msg = "Unable to process the supplied entity"
	}
	return ErrorResponse{
		Status:  http.StatusUnprocessableEntity,
		Message: msg,
	}
}

func TooManyRequests(msg string) ErrorResponse {
	if msg == "" {
		msg = "Too many requests"
	}
	return ErrorResponse{
		Status:  http.StatusTooManyRequests,
		Message: msg,
	}
}

type invalidField struct {
	Field string `json:"field"`
	Error string `json:"error"`
}

func InvalidInput(errs validation.Errors) ErrorResponse {
	var details []invalidField
	var fields []string
	for field := range errs {
		fields = append(fields, field)
	}
	sort.Strings(fields)
	for _, field := range fields {
		details = append(details, invalidField{
			Field: field,
			Error: errs[field].Error(),
		})
	}

	return ErrorResponse{
		Status:  http.StatusBadRequest,
		Message: "There is some problem with the data you submitted.",
		Details: details,
	}
}
