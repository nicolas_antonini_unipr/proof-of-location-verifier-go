package polreqquest

import (
	"context"
	"fmt"
	"math"
	"sort"
	"time"

	"org.nicolasantonini/verifier/internal/auth"
	"org.nicolasantonini/verifier/internal/entity"
	"org.nicolasantonini/verifier/internal/errors"
	"org.nicolasantonini/verifier/internal/log"
)

//The minimum number of available witnesses for a proof of location
var K = 2

type Service interface {
	Create(ctx context.Context) (PolId, error)
	Get(ctx context.Context) ([]PolId, error)
}

type PolId struct {
	entity.PolId
}

type service struct {
	repo   Repository
	logger log.Logger
}

func NewService(repo Repository, logger log.Logger) Service {
	return service{repo, logger}
}

func (s service) Get(ctx context.Context) ([]PolId, error) {
	//Gets the current user
	currentUser, err := s.repo.GetCurrentUser(ctx, auth.CurrentUser(ctx).GetName())
	if err != nil {
		return []PolId{}, err
	}

	//Gets the request for which the user has been selected as a witness
	requests, err := s.repo.GetRequestsForWitness(ctx, currentUser.ID)
	if err != nil {
		return []PolId{}, errors.InternalServerError("")
	}

	if len(requests) == 0 {
		return []PolId{}, errors.NotFound("user has not been selected for witnessing")
	}

	//Extracts the proof of location IDs
	requestsIds := make([]PolId, 0)
	for i := 0; i < len(requests); i++ {
		requestsIds = append(requestsIds, PolId{
			PolId: entity.PolId{
				Id: requests[i].PolId,
			},
		})
	}

	return requestsIds, nil
}

func (s service) Create(ctx context.Context) (PolId, error) {
	//Gets the current user
	currentUser, err := s.repo.GetCurrentUser(ctx, auth.CurrentUser(ctx).GetName())
	if err != nil {
		return PolId{}, err
	}

	//Checks the current location of the user
	checkin, err := s.repo.GetCheckinForUser(ctx, currentUser.ID)
	if err != nil {
		return PolId{}, errors.NotFound("unable to find a suitable checkin for the user")
	}

	//Checks for previous requests
	_, err = s.repo.GetRequestForUser(ctx, currentUser.ID)
	if err == nil {
		return PolId{}, errors.UnprocessableEntity("another request has been found")
	}

	//Gets every other user around
	nearbyCheckins, err := s.repo.GetCheckinsNear(ctx, checkin)
	if err != nil {
		return PolId{}, errors.InternalServerError("Unable to create the request")
	}

	//Selects the witnesses for this request
	if len(nearbyCheckins) < K {
		return PolId{}, errors.UnprocessableEntity("not enough witnesses around")
	}

	nearbyUsers := make([]entity.Users, 0)

	for i := 0; i < len(nearbyCheckins); i++ {
		nearbyUser, err := s.repo.GetUser(ctx, nearbyCheckins[i].UserID)
		if err != nil {
			return PolId{}, errors.InternalServerError("Unable to create the request")
		}
		nearbyUsers = append(nearbyUsers, nearbyUser)
	}

	selectedWitnesses, err := selectWitnesses(s, ctx, K, currentUser, nearbyUsers)
	if err != nil {
		return PolId{}, errors.UnprocessableEntity("Unable to find witnesses for this request")
	}

	selectedWitnessesIds := make([]string, 0)

	for i := 0; i < len(selectedWitnesses); i++ {
		selectedWitnessesIds = append(selectedWitnessesIds, nearbyUsers[i].ID)
	}

	//Creates the request
	pol_req := entity.PolRequest{
		ID:           entity.GenerateID(),
		PolId:        entity.Generate20byteID(),
		ProverId:     currentUser.ID,
		WitnessesIds: selectedWitnessesIds,
		CreatedAt:    time.Now(),
		ExpiresAt:    time.Now().Add(30 * time.Minute),
		Latitude:     checkin.Latitude,
		Longitude:    checkin.Longitude,
	}

	err = s.repo.Create(ctx, pol_req)
	if err != nil {
		return PolId{}, errors.InternalServerError("Unable to create the request")
	}

	return PolId{
		entity.PolId{
			Id: pol_req.PolId,
		},
	}, nil

}

func selectWitnesses(s service, ctx context.Context, k int, prover entity.Users, eligible_witnesses []entity.Users) ([]entity.Users, error) {

	//Calcolare Ew

	//Calcolare A(w, pi)
	selectedWitnesses := make([]entity.Users, 0)
	scores := make([]float64, 0)

	allPeers, err := s.repo.GetEveryUser(ctx)
	if err != nil {
		return nil, errors.InternalServerError("unable to select witnesses")
	}

	for j := 0; j < len(eligible_witnesses); j++ {

		currentWitness := eligible_witnesses[j]
		var ew float64 = 0.0

		totalNumberOfTxs, err := s.repo.GetTotalNumberOfTransactionsOfUser(ctx, currentWitness)
		if err != nil {
			return nil, errors.InternalServerError("unable to select witnesses")
		}

		for i := 0; i < len(allPeers); i++ {
			currentPeer := allPeers[i]
			if currentPeer.ID == currentWitness.ID {
				continue
			}

			numberOfTrxWithPeer, err := s.repo.GetNumberOfTransactionsBetweenUsers(ctx, currentWitness, currentPeer)
			if err != nil {
				return nil, errors.InternalServerError("unable to select witnesses")
			}

			if numberOfTrxWithPeer == 0 {
				continue
			}

			awp := float64(numberOfTrxWithPeer) / float64(totalNumberOfTxs)

			ewComponent := -awp * math.Log(awp)

			ew = ew + ewComponent
		}

		ep := 0.0
		fmt.Print(ep)
		totalNumberOfTxs, err = s.repo.GetTotalNumberOfTransactionsOfUser(ctx, prover)
		if err != nil {
			return nil, errors.InternalServerError("unable to select witnesses")
		}
		for i := 0; i < len(allPeers); i++ {
			currentPeer := allPeers[i]
			if currentPeer.ID == prover.ID {
				continue
			}
			numberOfTrxWithPeer, err := s.repo.GetNumberOfTransactionsBetweenUsers(ctx, prover, currentPeer)
			if err != nil {
				return nil, errors.InternalServerError("unable to select witnesses")
			}
			if numberOfTrxWithPeer == 0 {
				continue
			}
			awp := float64(numberOfTrxWithPeer) / float64(totalNumberOfTxs)
			epComponent := -awp * math.Log(awp)
			ep = ew + epComponent
		}

		trxBetweenUsers, err := s.repo.GetNumberOfTransactionsBetweenUsers(ctx, currentWitness, prover)
		if err != nil {
			return nil, errors.InternalServerError("unable to select witnesses")
		}

		score := (ew * ep) / (1 + float64(trxBetweenUsers))

		scores = append(scores, score)
	}

	//SORT SCORES!!!
	sortedScores := make([]float64, len(scores))
	copy(sortedScores, scores)
	sort.Float64s(sortedScores)
	//

	for i := 0; i < k; i++ {
		for j := 0; j < k; j++ {
			if sortedScores[i] == scores[j] {
				selectedWitnesses = append(selectedWitnesses, eligible_witnesses[j])
			}
		}
	}

	return selectedWitnesses, nil
}

//Get the distance, in meters, between two coordinates
func Distance(lat1, lon1, lat2, lon2 float64) float64 {
	var la1, lo1, la2, lo2, r float64
	la1 = lat1 * math.Pi / 180
	lo1 = lon1 * math.Pi / 180
	la2 = lat2 * math.Pi / 180
	lo2 = lon2 * math.Pi / 180

	r = 6378100

	h := hsin(la2-la1) + math.Cos(la1)*math.Cos(la2)*hsin(lo2-lo1)
	return 2 * r * math.Asin(math.Sqrt(h))
}

func hsin(theta float64) float64 {
	return math.Pow(math.Sin(theta/2), 2)
}
