package polreqquest

import (
	"net/http"

	routing "github.com/go-ozzo/ozzo-routing/v2"
	"org.nicolasantonini/verifier/internal/log"
)

func RegisterHandlers(r *routing.RouteGroup, service Service, authHandler routing.Handler, logger log.Logger) {
	res := resource{service, logger}
	r.Use(authHandler)
	r.Post("/polrequest", res.create)
	r.Get("/polrequest", res.get)
}

type resource struct {
	service Service
	logger  log.Logger
}

func (r resource) create(c *routing.Context) error {
	polId, err := r.service.Create(c.Request.Context())
	if err != nil {
		return err
	}

	return c.WriteWithStatus(polId, http.StatusCreated)
}

func (r resource) get(c *routing.Context) error {
	polIds, err := r.service.Get(c.Request.Context())
	if err != nil {
		return err
	}

	return c.WriteWithStatus(polIds, http.StatusOK)
}
