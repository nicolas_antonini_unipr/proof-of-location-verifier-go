package polreqquest

import (
	"context"
	"fmt"
	"time"

	dbx "github.com/go-ozzo/ozzo-dbx"
	"org.nicolasantonini/verifier/internal/checkin"
	"org.nicolasantonini/verifier/internal/dbcontext"
	"org.nicolasantonini/verifier/internal/entity"
	"org.nicolasantonini/verifier/internal/log"
)

type Repository interface {
	Get(ctx context.Context, name string) (entity.Users, error)
	GetRequestsForWitness(ctx context.Context, id string) ([]entity.PolRequest, error)
	Create(ctx context.Context, req entity.PolRequest) error
	GetCurrentUser(ctx context.Context, name string) (entity.Users, error)
	GetUser(ctx context.Context, id string) (entity.Users, error)
	GetCheckinForUser(ctx context.Context, user_id string) (entity.CheckIn, error)
	GetRequestForUser(ctx context.Context, user_id string) (entity.PolRequest, error)
	DeletePoLRequest(ctx context.Context, id string) error
	GetCheckinsNear(ctx context.Context, checkin entity.CheckIn) ([]entity.CheckIn, error)

	GetEveryUser(ctx context.Context) ([]entity.Users, error)
	GetTotalNumberOfTransactionsOfUser(ctx context.Context, user entity.Users) (int, error)
	GetNumberOfTransactionsBetweenUsers(ctx context.Context, user1 entity.Users, user2 entity.Users) (int, error)
}

type repository struct {
	db     *dbcontext.DB
	logger log.Logger
}

var PolRequests []entity.PolRequest

func NewRepository(db *dbcontext.DB, logger log.Logger) Repository {
	return repository{db, logger}
}

func (r repository) Get(ctx context.Context, name string) (entity.Users, error) {
	var user entity.Users
	err := r.db.With(ctx).Select().From("users").Where(dbx.HashExp{"name": name}).One(&user)
	return user, err
}

func (r repository) GetRequestsForWitness(ctx context.Context, id string) ([]entity.PolRequest, error) {
	requests := make([]entity.PolRequest, 0)
	for i := 0; i < len(PolRequests); i++ {
		for j := 0; j < len(PolRequests[i].WitnessesIds); j++ {
			if PolRequests[i].WitnessesIds[j] == id {
				requests = append(requests, PolRequests[i])
			}
		}
	}
	return requests, nil
}

func (r repository) Create(ctx context.Context, req entity.PolRequest) error {
	PolRequests = append(PolRequests, req)
	time.AfterFunc(time.Until(req.ExpiresAt), func() {
		var index int
		index = -1
		for i := 0; i < len(PolRequests); i++ {
			if PolRequests[i].ID == req.ID {
				index = i
				break
			}
		}
		if index != -1 {
			r.logger.Infof("Removing request at: %v", index)
			PolRequests = remove(PolRequests, index)
			return
		}
	})
	return nil
}

func (r repository) GetCurrentUser(ctx context.Context, name string) (entity.Users, error) {
	var currentUser entity.Users
	err := r.db.With(ctx).Select().From("users").Where(dbx.HashExp{"name": name}).One(&currentUser)
	return currentUser, err
}

func (r repository) GetUser(ctx context.Context, id string) (entity.Users, error) {
	var user entity.Users
	err := r.db.With(ctx).Select().From("users").Where(dbx.HashExp{"id": id}).One(&user)
	return user, err
}

func (r repository) GetCheckinForUser(ctx context.Context, user_id string) (entity.CheckIn, error) {
	for i := 0; i < len(checkin.CheckIns); i++ {
		if checkin.CheckIns[i].UserID == user_id && checkin.CheckIns[i].ExpiresAt.After(time.Now()) {
			return checkin.CheckIns[i], nil
		}
	}
	return entity.CheckIn{}, fmt.Errorf("not found")
}

func (r repository) GetRequestForUser(ctx context.Context, user_id string) (entity.PolRequest, error) {
	for i := 0; i < len(PolRequests); i++ {
		if PolRequests[i].ProverId == user_id {
			return PolRequests[i], nil
		}
	}
	return entity.PolRequest{}, fmt.Errorf("not found")
}

func (r repository) DeletePoLRequest(ctx context.Context, id string) error {
	var index int
	index = -1
	for i := 0; i < len(PolRequests); i++ {
		if PolRequests[i].ID == id {
			index = i
			break
		}
	}
	if index != -1 {
		PolRequests = remove(PolRequests, index)
		return nil
	} else {
		return fmt.Errorf("not found")
	}
}

func (r repository) GetCheckinsNear(ctx context.Context, chk entity.CheckIn) ([]entity.CheckIn, error) {
	var nearbyCheckins []entity.CheckIn
	for i := 0; i < len(checkin.CheckIns); i++ {
		if checkin.CheckIns[i].UserID != chk.UserID &&
			Distance(checkin.CheckIns[i].Latitude,
				checkin.CheckIns[i].Longitude,
				chk.Latitude,
				chk.Longitude) < 25.0 &&
			checkin.CheckIns[i].ExpiresAt.After(time.Now()) {
			nearbyCheckins = append(nearbyCheckins, checkin.CheckIns[i])
		}
	}
	return nearbyCheckins, nil
}

func remove(s []entity.PolRequest, i int) []entity.PolRequest {
	s[i] = s[len(s)-1]
	return s[:len(s)-1]
}

func (r repository) GetEveryUser(ctx context.Context) ([]entity.Users, error) {
	var users []entity.Users
	err := r.db.With(ctx).Select().From("users").All(&users)
	return users, err
}

func (r repository) GetTotalNumberOfTransactionsOfUser(ctx context.Context, user entity.Users) (int, error) {
	id1 := user.PubID1
	id2 := user.PubID2

	var transactions []entity.Transactions
	err := r.db.With(ctx).Select().
		From("transactions").
		Where(dbx.HashExp{"prover_id": id1}).
		OrWhere(dbx.HashExp{"prover_id": id2}).
		OrWhere(dbx.HashExp{"witness_id": id1}).
		OrWhere(dbx.HashExp{"witness_id": id2}).
		All(&transactions)

	return len(transactions), err
}

func (r repository) GetNumberOfTransactionsBetweenUsers(ctx context.Context, user1 entity.Users, user2 entity.Users) (int, error) {
	id11 := user1.PubID1
	id12 := user1.PubID2
	id21 := user2.PubID1
	id22 := user2.PubID2

	var transactionsU21ToU1 []entity.Transactions
	err := r.db.With(ctx).Select().
		From("transactions").
		Where(dbx.HashExp{"witness_id": id11}).
		OrWhere(dbx.HashExp{"witness_id": id12}).
		AndWhere(dbx.HashExp{"prover_id": id21}).
		All(&transactionsU21ToU1)

	if err != nil {
		return 0, err
	}

	var transactionsU22ToU1 []entity.Transactions
	err = r.db.With(ctx).Select().
		From("transactions").
		Where(dbx.HashExp{"witness_id": id11}).
		OrWhere(dbx.HashExp{"witness_id": id12}).
		AndWhere(dbx.HashExp{"prover_id": id22}).
		All(&transactionsU22ToU1)

	if err != nil {
		return 0, err
	}

	var transactionsU11ToU2 []entity.Transactions
	err = r.db.With(ctx).Select().
		From("transactions").
		Where(dbx.HashExp{"witness_id": id21}).
		OrWhere(dbx.HashExp{"witness_id": id22}).
		AndWhere(dbx.HashExp{"prover_id": id11}).
		All(&transactionsU11ToU2)

	if err != nil {
		return 0, err
	}

	var transactionsU12ToU2 []entity.Transactions
	err = r.db.With(ctx).Select().
		From("transactions").
		Where(dbx.HashExp{"witness_id": id21}).
		OrWhere(dbx.HashExp{"witness_id": id22}).
		AndWhere(dbx.HashExp{"prover_id": id12}).
		All(&transactionsU12ToU2)

	return len(transactionsU21ToU1) + len(transactionsU22ToU1) + len(transactionsU11ToU2) + len(transactionsU12ToU2), err
}
