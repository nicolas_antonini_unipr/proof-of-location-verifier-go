package pseudonyms

import (
	routing "github.com/go-ozzo/ozzo-routing/v2"
	"org.nicolasantonini/verifier/internal/log"
)

func RegisterHandlers(r *routing.RouteGroup, service Service, authHandler routing.Handler, logger log.Logger) {
	res := resource{service, logger}

	r.Use(authHandler)

	// the following endpoints require a valid JWT
	r.Post("/pseudonym", res.update)
	r.Get("/pseudonym", res.get)
}

type resource struct {
	service Service
	logger  log.Logger
}

func (r resource) update(c *routing.Context) error {
	pk, err := r.service.Update(c.Request.Context())
	if err != nil {
		return err
	}

	return c.Write(pk)
}

func (r resource) get(c *routing.Context) error {
	pk, err := r.service.Get(c.Request.Context())
	if err != nil {
		return err
	}

	return c.Write(pk)
}
