package pseudonyms

import (
	"context"
	"time"

	"org.nicolasantonini/verifier/internal/auth"
	"org.nicolasantonini/verifier/internal/entity"
	"org.nicolasantonini/verifier/internal/errors"
	"org.nicolasantonini/verifier/internal/log"
)

type Service interface {
	Get(ctx context.Context) (Pubkey, error)
	Update(ctx context.Context) (Pubkey, error)
}

type Pubkey struct {
	entity.Pubkey
}

type service struct {
	repo   Repository
	logger log.Logger
}

func NewService(repo Repository, logger log.Logger) Service {
	return service{repo, logger}
}

func (s service) Update(ctx context.Context) (Pubkey, error) {
	uid := auth.CurrentUser(ctx).GetID()
	user, err := s.repo.Get(ctx, uid)
	if err != nil {
		return Pubkey{}, err
	}

	if time.Since(user.LastUpdate) < (time.Hour * 24) {
		return Pubkey{}, errors.TooManyRequests("")
	}

	user.PubID1 = user.PubID2
	user.PubID2 = entity.Generate16byteID()
	user.LastUpdate = time.Now()

	if err := s.repo.Update(ctx, user); err != nil {
		return Pubkey{}, err
	}
	var pk = Pubkey{
		entity.Pubkey{
			KeyString: user.PubID2,
		},
	}
	return pk, nil
}

func (s service) Get(ctx context.Context) (Pubkey, error) {
	currentUser := auth.CurrentUser(ctx)
	user, err := s.repo.Get(ctx, currentUser.GetID())
	if err != nil {
		return Pubkey{}, err
	}
	pubKey := user.PubID2
	var pk = Pubkey{
		entity.Pubkey{
			KeyString: pubKey,
		},
	}
	return pk, nil
}
