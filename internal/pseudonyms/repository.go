package pseudonyms

import (
	"context"

	"org.nicolasantonini/verifier/internal/dbcontext"
	"org.nicolasantonini/verifier/internal/entity"
	"org.nicolasantonini/verifier/internal/log"
)

type Repository interface {
	Get(ctx context.Context, id string) (entity.Users, error)
	Update(ctx context.Context, album entity.Users) error
}

type repository struct {
	db     *dbcontext.DB
	logger log.Logger
}

func NewRepository(db *dbcontext.DB, logger log.Logger) Repository {
	return repository{db, logger}
}

func (r repository) Get(ctx context.Context, id string) (entity.Users, error) {
	var user entity.Users
	err := r.db.With(ctx).Select().Model(id, &user)
	return user, err
}

// Update saves the changes to an album in the database.
func (r repository) Update(ctx context.Context, user entity.Users) error {
	return r.db.With(ctx).Model(&user).Update()
}
