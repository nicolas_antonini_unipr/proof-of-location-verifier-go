package auth

import (
	"context"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"org.nicolasantonini/verifier/internal/entity"
	"org.nicolasantonini/verifier/internal/errors"
	"org.nicolasantonini/verifier/internal/log"
)

type Service interface {
	Login(ctx context.Context, username, password string) (string, error)
	SignUp(ctx context.Context, username, password, publickey string) error
}

type Identity interface {
	GetID() string
	GetName() string
	GetPubID1() string
	GetPubID2() string
}

type service struct {
	signingKey      string
	tokenExpiration int
	logger          log.Logger
	repo            Repository
}

func NewService(signingKey string, tokenExpiration int, repo Repository, logger log.Logger) Service {
	return service{signingKey, tokenExpiration, logger, repo}
}

func (s service) Login(ctx context.Context, username, password string) (string, error) {
	identity, err := s.authenticate(ctx, username, password)
	if err != nil {
		return "", err
	}

	token, err := s.generateJWT(identity)
	if err != nil {
		return "", err
	}

	return token, nil
}

func (s service) SignUp(ctx context.Context, username, password, publickey string) error {
	if username == "" || password == "" || publickey == "" {
		return errors.BadRequest("")
	}
	if err := s.register(ctx, username, password, publickey); err != nil {
		return err
	}

	return nil
}

func (s service) authenticate(ctx context.Context, username, password string) (Identity, error) {
	user, err := s.repo.Get(ctx, username)
	if err != nil {
		return nil, errors.NotFound("user not found")
	}

	if user.Password == password {
		return user, nil
	}

	return nil, errors.Unauthorized("wrong password")

}

func (s service) register(ctx context.Context, username, password, publickey string) error {
	_, err := s.repo.Get(ctx, username)
	if err == nil {
		return errors.UnprocessableEntity("user already exists")
	}

	id := entity.GenerateID()
	name := username
	pub_id1 := entity.Generate16byteID()
	pub_id2 := entity.Generate16byteID()
	pass := password

	e := s.repo.Create(ctx, entity.Users{
		ID:         id,
		Name:       name,
		PubID1:     pub_id1,
		PubID2:     pub_id2,
		Password:   pass,
		LastUpdate: time.Now(),
		PublicKey:  publickey,
	})

	if e != nil {
		return errors.InternalServerError("unable to create the user")
	}

	return nil
}

func (s service) generateJWT(identity Identity) (string, error) {
	return jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"id":   identity.GetID(),
		"name": identity.GetName(),
		"exp":  time.Now().Add(time.Duration(s.tokenExpiration) * time.Hour).Unix(),
	}).SignedString([]byte(s.signingKey))
}
