package auth

import (
	"context"

	dbx "github.com/go-ozzo/ozzo-dbx"
	"org.nicolasantonini/verifier/internal/dbcontext"
	"org.nicolasantonini/verifier/internal/entity"
	"org.nicolasantonini/verifier/internal/log"
)

type Repository interface {
	Get(ctx context.Context, name string) (entity.Users, error)
	Create(ctx context.Context, album entity.Users) error
}

type repository struct {
	db     *dbcontext.DB
	logger log.Logger
}

func NewRepository(db *dbcontext.DB, logger log.Logger) Repository {
	return repository{db, logger}
}

//Gets the user with the specified username
func (r repository) Get(ctx context.Context, name string) (entity.Users, error) {
	var user entity.Users
	err := r.db.With(ctx).Select().From("users").Where(dbx.HashExp{"name": name}).One(&user)
	return user, err
}

//Creates a new user
func (r repository) Create(ctx context.Context, user entity.Users) error {
	return r.db.With(ctx).Model(&user).Insert()
}
