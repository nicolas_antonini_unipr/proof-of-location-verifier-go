package auth

import (
	"net/http"

	routing "github.com/go-ozzo/ozzo-routing/v2"
	"org.nicolasantonini/verifier/internal/errors"
	"org.nicolasantonini/verifier/internal/log"
)

//HTTP handlers for authentication service
func RegisterHandlers(rg *routing.RouteGroup, service Service, logger log.Logger) {
	rg.Post("/login", login(service, logger))
	rg.Post("/signup", signup(service, logger))
}

//Handles a login request from a registered user
func login(service Service, logger log.Logger) routing.Handler {
	return func(c *routing.Context) error {
		type LoginRequest struct {
			Username string `json:"username"`
			Password string `json:"password"`
		}

		var req LoginRequest

		if err := c.Read(&req); err != nil {
			logger.With(c.Request.Context()).Errorf("invalid request: %v", err)
			return errors.BadRequest("")
		}

		token, err := service.Login(c.Request.Context(), req.Username, req.Password)
		if err != nil {
			logger.With(c.Request.Context()).Errorf("unable to login user: %v", err)
			return err
		}

		return c.Write(struct {
			Token string `json:"token"`
		}{token})
	}
}

//Handles a signup request from a new user
func signup(service Service, logger log.Logger) routing.Handler {
	return func(c *routing.Context) error {
		var req struct {
			Username  string `json:"username"`
			Password  string `json:"password"`
			PublicKey string `json:"public-key"`
		}

		if err := c.Read(&req); err != nil {
			logger.With(c.Request.Context()).Errorf("invalid request: %v", err)
			return errors.BadRequest("")
		}

		err := service.SignUp(c.Request.Context(), req.Username, req.Password, req.PublicKey)

		if err != nil {
			logger.With(c.Request.Context()).Errorf("unable to register user: %v", err)
			return err
		}

		return c.Write(http.StatusOK)
	}
}
