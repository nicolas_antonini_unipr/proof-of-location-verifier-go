package proofoflocation

import (
	"context"
	"encoding/base64"
	"fmt"
	"math"
	"time"

	"crypto/aes"
	"crypto/cipher"

	"crypto/rsa"
	"crypto/x509"
	"encoding/binary"

	jwt "github.com/dgrijalva/jwt-go"
	"org.nicolasantonini/verifier/internal/auth"
	"org.nicolasantonini/verifier/internal/certificates"
	"org.nicolasantonini/verifier/internal/entity"
	"org.nicolasantonini/verifier/internal/errors"
	"org.nicolasantonini/verifier/internal/log"
	"org.nicolasantonini/verifier/internal/smartcontract"
)

type Service interface {
	Create(ctx context.Context, input CreatePolRequest) (string, error)
}

type CreatePolRequest struct {
	PolId     string `json:"pol-id"`
	PolResult string `json:"result"`
}

type service struct {
	repo   Repository
	logger log.Logger
	key    string
}

func NewService(repo Repository, logger log.Logger, key string) Service {
	return service{repo, logger, key}
}

type SplittedResult struct {
	stringR          []byte
	stringC          []byte
	stringH          []byte
	witnessID        string
	witnessLatitude  float64
	witnessLongitude float64
	witnessTimestamp float64
	witnessSignature []byte
	stringA          []byte
	stringB          []byte
	proverID         string
	proverLatitude   float64
	proverLongitude  float64
	proverSignature  []byte
}

func (s service) Create(ctx context.Context, input CreatePolRequest) (string, error) {
	currentUser, err := s.repo.GetCurrentUser(ctx, auth.CurrentUser(ctx).GetName())
	if err != nil {
		return "", errors.Unauthorized("Unable to find current user")
	}

	polRequest, err := s.repo.GetPolRequest(ctx, input.PolId)
	if err != nil {
		return "", errors.NotFound("Unable to find a request for this ID")
	}

	if polRequest.ProverId != currentUser.ID {
		return "", errors.Unauthorized("Request was not created by user")
	}

	if len(polRequest.WitnessesIds) == 0 {
		return "", errors.InternalServerError("The request is in an invalid state")
	}

	decodedResult, err := decodeResponse(input.PolResult)
	if err != nil {
		return "", errors.BadRequest("The proof of location is not valid")
	}

	witnessPseudonym, err := validateResponse(decodedResult, polRequest, currentUser)
	if err != nil {
		return "", errors.BadRequest("The proof of location is not valid")
	}

	witness, err := s.repo.GetWitnessByPseudonym(ctx, witnessPseudonym)
	if err != nil {
		return "", errors.UnprocessableEntity("The witness pseudonym is not valid")
	}

	var index int
	index = -1
	for i := 0; i < len(polRequest.WitnessesIds); i++ {
		if polRequest.WitnessesIds[i] == witness.ID {
			index = i
			break
		}
	}
	if index != -1 {
		polRequest.WitnessesIds = removeWitness(polRequest.WitnessesIds, index)
	} else {
		return "", errors.UnprocessableEntity("The witness pseudonym is not valid")
	}

	err = s.repo.UpdatePolRequest(ctx, polRequest)
	if err != nil {
		return "", errors.InternalServerError("Unable verify the proof of location")
	}

	err = s.repo.CreateTransaction(ctx, entity.Transactions{
		ID:        entity.GenerateID(),
		PolID:     polRequest.PolId,
		WitnessID: witnessPseudonym,
		ProverID:  polRequest.ProverId,
	})
	if err != nil {
		return "", errors.InternalServerError("Unable to store the transaction")
	}

	//K - T
	if len(polRequest.WitnessesIds) <= (3 - 1) {

		err = smartcontract.PostProofOfLocation(currentUser.PubID2, polRequest.Latitude, polRequest.Longitude, polRequest.CreatedAt)
		if err != nil {
			log.New().Error("unable to write the proof of location on the blockchain")
		}

		return jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
			"pseudonym": currentUser.PubID2,
			"latitude":  polRequest.Latitude,
			"longitude": polRequest.Longitude,
			"timestamp": polRequest.CreatedAt,
		}).SignedString([]byte(s.key))
	} else {
		return "", nil
	}
}

func removeWitness(s []string, i int) []string {
	s[i] = s[len(s)-1]
	return s[:len(s)-1]
}

func validateResponse(response SplittedResult, request entity.PolRequest, prover entity.Users) (string, error) {
	if Distance(response.witnessLatitude, response.witnessLongitude, request.Latitude, request.Longitude) > 100 {
		return "", fmt.Errorf("the location is not valid")
	}
	if response.proverID != prover.PubID2 {
		return "", fmt.Errorf("the prover id is not valid")
	}
	if time.Until(time.Unix(0, int64(response.witnessTimestamp)*int64(time.Millisecond))) < time.Minute {
		return "", fmt.Errorf("the timestamp is not valid")
	}

	return response.witnessID, nil
}

func decodeResponse(response string) (SplittedResult, error) {
	responseData, err := base64.StdEncoding.DecodeString(response)
	if err != nil {
		return SplittedResult{}, fmt.Errorf("unable to decode base64-encoded proof of location response")
	}

	verifierPrivateKeyBase64String := certificates.PrivateRSAKey
	verifierPrivateKey, err := ParseRsaPrivateKeyFromPemStr(verifierPrivateKeyBase64String)

	if err != nil {
		return SplittedResult{}, fmt.Errorf("error parsing verifier private key")
	}

	si := len(responseData) - 3 - 64
	ei := len(responseData) - 3
	proofOfLocationAESEncryptionKeyCT := responseData[si:ei]

	si = 0
	ei = len(responseData) - 3 - 64 - 3
	proofOfLocationCT := responseData[si:ei]

	proofOfLocationAESEncryptionKeyPT, err := rsa.DecryptPKCS1v15(nil, verifierPrivateKey, proofOfLocationAESEncryptionKeyCT)
	if err != nil {
		return SplittedResult{}, fmt.Errorf("unable to decrypt aes key")
	}

	proofOfLocationPT := Ase256Decode(proofOfLocationCT, proofOfLocationAESEncryptionKeyPT)

	si = 0
	ei = 8
	stringR := proofOfLocationPT[si:ei]

	si = si + 8
	ei = ei + 8
	stringC := proofOfLocationPT[si:ei]

	si = si + 8
	ei = ei + 8
	stringH := proofOfLocationPT[si:ei]

	si = si + 8
	ei = ei + 16
	witnessID := proofOfLocationPT[si:ei]

	si = si + 16
	ei = ei + 8
	witnessLatitude := proofOfLocationPT[si:ei]

	si = si + 8
	ei = ei + 8
	witnessLongitude := proofOfLocationPT[si:ei]

	si = si + 8
	ei = ei + 8
	witnessTimestamp := proofOfLocationPT[si:ei]

	witnessSignature := proofOfLocationPT[len(proofOfLocationPT)-64:]

	si = si + 8 + 3
	ei = ei + 128 + 3
	messageECT := proofOfLocationPT[si:ei]

	si = si + 128 + 3
	ei = ei + 3 + 64
	messageEAESEncryptionKeyCT := proofOfLocationPT[si:ei]

	messageEAESEncryptionKeyPT, err := rsa.DecryptPKCS1v15(nil, verifierPrivateKey, messageEAESEncryptionKeyCT)
	if err != nil {
		return SplittedResult{}, fmt.Errorf("unable to decrypt aes key")
	}

	messageEPT := Ase256Decode(messageECT, messageEAESEncryptionKeyPT)

	si = 0
	ei = 8
	stringA := messageEPT[si:ei]

	si = si + 8
	ei = ei + 8
	stringB := messageEPT[si:ei]

	si = si + 8
	ei = ei + 16
	proverID := messageEPT[si:ei]

	si = si + 16
	ei = ei + 8
	proverLatitude := messageEPT[si:ei]

	si = si + 8
	ei = ei + 8
	proverLongitude := messageEPT[si:ei]

	proverSignature := messageEPT[len(messageEPT)-64:]

	return SplittedResult{
		stringR:          stringR,
		stringC:          stringC,
		stringH:          stringH,
		witnessID:        string(witnessID),
		witnessLatitude:  Float64frombytes(witnessLatitude),
		witnessLongitude: Float64frombytes(witnessLongitude),
		witnessTimestamp: Float64frombytes(witnessTimestamp),
		witnessSignature: witnessSignature,
		stringA:          stringA,
		stringB:          stringB,
		proverID:         string(proverID),
		proverLatitude:   Float64frombytes(proverLatitude),
		proverLongitude:  Float64frombytes(proverLongitude),
		proverSignature:  proverSignature,
	}, nil
}

func Float64frombytes(bytes []byte) float64 {
	bits := binary.LittleEndian.Uint64(bytes)
	float := math.Float64frombits(bits)
	return float
}

func ParseRsaPrivateKeyFromPemStr(privPEM string) (*rsa.PrivateKey, error) {
	pemData, err := base64.StdEncoding.DecodeString(privPEM)
	if err != nil {
		return nil, err
	}
	priv, err := x509.ParsePKCS8PrivateKey(pemData)
	if err != nil {
		return nil, err
	}

	return priv.(*rsa.PrivateKey), nil
}

func ParseRsaPublicKeyFromPemStr(pubPEM string) (*rsa.PublicKey, error) {
	pemData, err := base64.StdEncoding.DecodeString(pubPEM)
	if err != nil {
		return nil, err
	}

	pub, err := x509.ParsePKIXPublicKey(pemData)
	if err != nil {
		return nil, err
	}

	switch pub := pub.(type) {
	case *rsa.PublicKey:
		return pub, nil
	default:
		break
	}
	return nil, fmt.Errorf("key type is not RSA")
}

func Ase256Decode(cipherText []byte, encKey []byte) (decryptedString []byte) {
	bKey := encKey
	bIV := make([]byte, 16)
	cipherTextDecoded := cipherText

	block, err := aes.NewCipher(bKey)
	if err != nil {
		panic(err)
	}

	mode := cipher.NewCBCDecrypter(block, bIV)
	mode.CryptBlocks([]byte(cipherTextDecoded), []byte(cipherTextDecoded))
	return cipherTextDecoded
}

//Get the distance, in meters, between two coordinates
func Distance(lat1, lon1, lat2, lon2 float64) float64 {
	var la1, lo1, la2, lo2, r float64
	la1 = lat1 * math.Pi / 180
	lo1 = lon1 * math.Pi / 180
	la2 = lat2 * math.Pi / 180
	lo2 = lon2 * math.Pi / 180

	r = 6378100

	h := hsin(la2-la1) + math.Cos(la1)*math.Cos(la2)*hsin(lo2-lo1)
	return 2 * r * math.Asin(math.Sqrt(h))
}

func hsin(theta float64) float64 {
	return math.Pow(math.Sin(theta/2), 2)
}
