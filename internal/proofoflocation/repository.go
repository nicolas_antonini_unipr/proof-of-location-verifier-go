package proofoflocation

import (
	"context"
	"fmt"

	dbx "github.com/go-ozzo/ozzo-dbx"
	"org.nicolasantonini/verifier/internal/dbcontext"
	"org.nicolasantonini/verifier/internal/entity"
	"org.nicolasantonini/verifier/internal/log"
	"org.nicolasantonini/verifier/internal/polreqquest"
)

type Repository interface {
	Get(ctx context.Context, name string) (entity.ProofOfLocation, error)
	Create(ctx context.Context, req entity.ProofOfLocation) error
	GetCurrentUser(ctx context.Context, name string) (entity.Users, error)
	GetUser(ctx context.Context, id string) (entity.Users, error)
	GetWitnessByPseudonym(ctx context.Context, pseudonym string) (entity.Users, error)
	GetPolRequest(ctx context.Context, pol_id string) (entity.PolRequest, error)
	UpdatePolRequest(ctx context.Context, pol_request entity.PolRequest) error
	CreateTransaction(ctx context.Context, transaction entity.Transactions) error
}

type repository struct {
	db     *dbcontext.DB
	logger log.Logger
}

func NewRepository(db *dbcontext.DB, logger log.Logger) Repository {
	return repository{db, logger}
}

func (r repository) Get(ctx context.Context, name string) (entity.ProofOfLocation, error) {
	return entity.ProofOfLocation{}, nil
}

func (r repository) Create(ctx context.Context, req entity.ProofOfLocation) error {
	return nil
}

func (r repository) GetCurrentUser(ctx context.Context, name string) (entity.Users, error) {
	var currentUser entity.Users
	err := r.db.With(ctx).Select().From("users").Where(dbx.HashExp{"name": name}).One(&currentUser)
	return currentUser, err
}

func (r repository) GetUser(ctx context.Context, id string) (entity.Users, error) {
	return entity.Users{}, nil
}

func (r repository) GetWitnessByPseudonym(ctx context.Context, pseudonym string) (entity.Users, error) {
	var user entity.Users
	err := r.db.With(ctx).Select().From("users").Where(dbx.HashExp{"pub_id2": pseudonym}).One(&user)
	return user, err
}

func (r repository) GetPolRequest(ctx context.Context, pol_id string) (entity.PolRequest, error) {
	requests := polreqquest.PolRequests
	for i := 0; i < len(requests); i++ {
		if requests[i].PolId == pol_id {
			return requests[i], nil
		}
	}
	return entity.PolRequest{}, fmt.Errorf("unable to find a request with the given pol-id")
}

func (r repository) UpdatePolRequest(ctx context.Context, pol_request entity.PolRequest) error {
	requests := polreqquest.PolRequests
	var index int
	index = -1
	for i := 0; i < len(requests); i++ {
		if requests[i].ID == pol_request.ID {
			index = i
			break
		}
	}
	if index != -1 {
		polreqquest.PolRequests = remove(polreqquest.PolRequests, index)
		polreqquest.PolRequests = append(polreqquest.PolRequests, pol_request)
		return nil
	} else {
		return nil
	}
}

func (r repository) CreateTransaction(ctx context.Context, transaction entity.Transactions) error {
	return r.db.With(ctx).Model(&transaction).Insert()
}

func remove(s []entity.PolRequest, i int) []entity.PolRequest {
	s[i] = s[len(s)-1]
	return s[:len(s)-1]
}
