package proofoflocation

import (
	"net/http"

	routing "github.com/go-ozzo/ozzo-routing/v2"
	"org.nicolasantonini/verifier/internal/errors"
	"org.nicolasantonini/verifier/internal/log"
)

func RegisterHandlers(r *routing.RouteGroup, service Service, authHandler routing.Handler, logger log.Logger) {
	res := resource{service, logger}
	r.Use(authHandler)
	// the following endpoints require a valid JWT
	r.Post("/proof-of-location", res.create)
}

type resource struct {
	service Service
	logger  log.Logger
}

func (r resource) create(c *routing.Context) error {
	var input CreatePolRequest
	if err := c.Read(&input); err != nil {
		r.logger.With(c.Request.Context()).Info(err)
		return errors.BadRequest("")
	}

	tok, err := r.service.Create(c.Request.Context(), input)
	if err != nil {
		return err
	}

	if tok != "" {
		return c.WriteWithStatus(tok, http.StatusOK)
	} else {
		return c.Write(http.StatusAccepted)
	}
}
