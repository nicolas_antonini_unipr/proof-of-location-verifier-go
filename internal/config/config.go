package config

import (
	"io/ioutil"

	"org.nicolasantonini/verifier/internal/log"

	vali "github.com/go-ozzo/ozzo-validation/v4"
	yaml "gopkg.in/yaml.v2"
)

type Config struct {
	ServerPort        int    `yaml:"serverport"`
	DSN               string `yaml:"dsn"`
	JWTPrivateKey     string `yaml:"jwt_private_key"`
	JWTExpiration     int    `yaml:"jwt_expiration"`
	AlgodAddress      string `yaml:"algod_address"`
	KmdAddress        string `yaml:"kmd_address"`
	AlgodToken        string `yaml:"algod_token"`
	KmdToken          string `yaml:"kmd_token"`
	AccountPrivateKey string `yaml:"algo_pvt_key"`
	AppIdx            int    `yaml:"app_id"`
}

func (conf Config) Validate() error {
	return vali.ValidateStruct(&conf,
		vali.Field(&conf.DSN, vali.Required),
		vali.Field(&conf.JWTPrivateKey, vali.Required),
	)
}

func Load(file string, logger log.Logger) (*Config, error) {
	//Default configuration
	conf := Config{
		ServerPort:        8080,
		JWTExpiration:     72,
		AccountPrivateKey: "ecQ9YF83whq02UysiqcApDcTB36XG0ocFHFYk9/jfwXZOl5ePWOOrS841Tz1T+pCHgNP1zcnb+3RdKj1Zy1I2g",
		AlgodAddress:      "http://localhost:4001",
		AlgodToken:        "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
		KmdAddress:        "http://localhost:4002",
		KmdToken:          "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
		AppIdx:            18,
	}

	bytes, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, err
	}

	if err = yaml.Unmarshal(bytes, &conf); err != nil {
		return nil, err
	}

	if err = conf.Validate(); err != nil {
		return nil, err
	}

	return &conf, err

}
