package checkin

import (
	"context"
	"fmt"
	"time"

	dbx "github.com/go-ozzo/ozzo-dbx"
	"org.nicolasantonini/verifier/internal/dbcontext"
	"org.nicolasantonini/verifier/internal/entity"
	"org.nicolasantonini/verifier/internal/log"
)

type Repository interface {
	Get(ctx context.Context, id string) (entity.CheckIn, error)
	GetCurrentUser(ctx context.Context, name string) (entity.Users, error)
	GetCheckinForUser(ctx context.Context, user_pubkey string) (entity.CheckIn, error)
	Create(ctx context.Context, checkin entity.CheckIn) error
	Delete(ctx context.Context, id string) error
}

type repository struct {
	db     *dbcontext.DB
	logger log.Logger
}

func NewRepository(db *dbcontext.DB, logger log.Logger) Repository {
	return repository{db, logger}
}

var CheckIns []entity.CheckIn

func (r repository) Get(ctx context.Context, id string) (entity.CheckIn, error) {
	for i := 0; i < len(CheckIns); i++ {
		if CheckIns[i].ID == id {
			return CheckIns[i], nil
		}
	}
	return entity.CheckIn{}, fmt.Errorf("not found")
}

func (r repository) GetCurrentUser(ctx context.Context, name string) (entity.Users, error) {
	var currentUser entity.Users
	err := r.db.With(ctx).Select().From("users").Where(dbx.HashExp{"name": name}).One(&currentUser)
	return currentUser, err
}

func (r repository) GetCheckinForUser(ctx context.Context, user_pubkey string) (entity.CheckIn, error) {
	for i := 0; i < len(CheckIns); i++ {
		if CheckIns[i].UserID == user_pubkey && CheckIns[i].ExpiresAt.After(time.Now()) {
			return CheckIns[i], nil
		}
	}
	return entity.CheckIn{}, fmt.Errorf("not found")
}

func (r repository) Create(ctx context.Context, checkin entity.CheckIn) error {
	CheckIns = append(CheckIns, checkin)
	//Sets a timer to delete the checkin after its expiration
	time.AfterFunc(time.Until(checkin.ExpiresAt), func() {
		var index int
		index = -1
		for i := 0; i < len(CheckIns); i++ {
			if CheckIns[i].ID == checkin.ID {
				index = i
				break
			}
		}
		if index != -1 {
			r.logger.Infof("Removing checkin at: %v", index)
			CheckIns = remove(CheckIns, index)
			return
		}
	})
	return nil
}

func (r repository) Delete(ctx context.Context, id string) error {
	var index int
	index = -1
	for i := 0; i < len(CheckIns); i++ {
		if CheckIns[i].ID == id {
			index = i
			break
		}
	}
	if index != -1 {
		CheckIns = remove(CheckIns, index)
		return nil
	} else {
		return fmt.Errorf("not found")
	}
}

func remove(s []entity.CheckIn, i int) []entity.CheckIn {
	s[i] = s[len(s)-1]
	return s[:len(s)-1]
}
