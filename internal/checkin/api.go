package checkin

import (
	"net/http"

	routing "github.com/go-ozzo/ozzo-routing/v2"
	"org.nicolasantonini/verifier/internal/errors"
	"org.nicolasantonini/verifier/internal/log"
)

func RegisterHandlers(r *routing.RouteGroup, service Service, authHandler routing.Handler, logger log.Logger) {
	res := resource{service, logger}

	r.Use(authHandler)

	// the following endpoints require a valid JWT
	r.Post("/checkin", res.create)
	r.Delete("/checkin/<id>", res.delete)
}

type resource struct {
	service Service
	logger  log.Logger
}

func (r resource) create(c *routing.Context) error {
	var input CreateCheckInRequest
	if err := c.Read(&input); err != nil {
		r.logger.With(c.Request.Context()).Info(err)
		return errors.BadRequest("")
	}
	checkin, err := r.service.Create(c.Request.Context(), input)
	if err != nil {
		return err
	}

	return c.WriteWithStatus(checkin, http.StatusCreated)
}

func (r resource) delete(c *routing.Context) error {
	checkin, err := r.service.Delete(c.Request.Context(), c.Param("id"))
	if err != nil {
		return err
	}

	return c.Write(checkin)
}
