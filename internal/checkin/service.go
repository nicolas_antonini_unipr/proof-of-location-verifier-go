package checkin

import (
	"context"
	"math"
	"time"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"org.nicolasantonini/verifier/internal/auth"
	"org.nicolasantonini/verifier/internal/entity"
	"org.nicolasantonini/verifier/internal/errors"
	"org.nicolasantonini/verifier/internal/log"
)

// Service encapsulates usecase logic for checkins.
type Service interface {
	//Gets a checkin from its id
	Get(ctx context.Context, id string) (CheckIn, error)
	//Deletes a checkin
	Delete(ctx context.Context, id string) (CheckIn, error)
	//Creates a checkin
	Create(ctx context.Context, input CreateCheckInRequest) (CheckIn, error)
}

type CheckIn struct {
	entity.CheckIn
}

type CreateCheckInRequest struct {
	Latitude   float64   `json:"latitude"`
	Longitude  float64   `json:"longitude"`
	Timestamp  time.Time `json:"timestamp"`
	TimeToLive int       `json:"time-to-live"`
}

func (m CreateCheckInRequest) Validate() error {
	return validation.ValidateStruct(&m,
		validation.Field(&m.Latitude, validation.Required),
		validation.Field(&m.Longitude, validation.Required),
		validation.Field(&m.Timestamp, validation.Required),
		validation.Field(&m.TimeToLive, validation.Required),
	)
}

type service struct {
	repo   Repository
	logger log.Logger
}

func NewService(repo Repository, logger log.Logger) Service {
	return service{repo, logger}
}

func (s service) Get(ctx context.Context, id string) (CheckIn, error) {
	checkin, err := s.repo.Get(ctx, id)
	if err != nil {
		return CheckIn{}, err
	}
	return CheckIn{checkin}, nil
}

func (s service) Create(ctx context.Context, input CreateCheckInRequest) (CheckIn, error) {
	//Validates the checkin request
	if err := input.Validate(); err != nil {
		return CheckIn{}, errors.BadRequest("request is in an invalid format")
	}

	//Gets the current user
	currentUser, err := s.repo.GetCurrentUser(ctx, auth.CurrentUser(ctx).GetName())
	if err != nil {
		return CheckIn{}, errors.Unauthorized("user not found")
	}

	//Checks the existence of previous checkins
	previousCheckin, err := s.repo.GetCheckinForUser(ctx, currentUser.ID)
	if err == nil {

		//Checks if the distance between the two checkins is valid
		if Distance(previousCheckin.Latitude,
			previousCheckin.Longitude,
			input.Latitude,
			input.Longitude) > 1000.0 {
			return CheckIn{}, errors.UnprocessableEntity("An incompatible checkin has been found.")
		} else {
			s.Delete(ctx, previousCheckin.ID)
		}
	}

	//Checks if the timestamp is valid
	if time.Since(input.Timestamp) < -(time.Second*60) || time.Since(input.Timestamp) > (time.Second*60) {
		return CheckIn{}, errors.UnprocessableEntity("Server and client are out of sync.")
	}

	//Creates the new checkin
	id := entity.GenerateID()
	now := time.Now()
	uid := currentUser.ID
	exp := now.Add(time.Second * time.Duration(math.Min(1800, float64(input.TimeToLive))))
	lat := input.Latitude
	lon := input.Longitude

	err = s.repo.Create(ctx, entity.CheckIn{
		ID:           id,
		UserID:       uid,
		Latitude:     lat,
		Longitude:    lon,
		RegisteredAt: now,
		ExpiresAt:    exp,
	})

	if err != nil {
		return CheckIn{}, errors.InternalServerError("")
	}

	//Returns the created checkin
	return s.Get(ctx, id)

}

func (s service) Delete(ctx context.Context, id string) (CheckIn, error) {
	//Gets the specified checkin
	checkin, err := s.Get(ctx, id)
	if err != nil {
		return CheckIn{}, errors.NotFound("Checkin not found")
	}

	//Checks if the user owns the specified checkin
	if checkin.UserID != auth.CurrentUser(ctx).GetID() {
		return CheckIn{}, errors.Forbidden("The specified checkin was not created by the current user")
	}

	//Deletes the checkin
	if err = s.repo.Delete(ctx, id); err != nil {
		return CheckIn{}, err
	}

	return checkin, nil
}

//Get the distance, in meters, between two coordinates
func Distance(lat1, lon1, lat2, lon2 float64) float64 {
	var la1, lo1, la2, lo2, r float64
	la1 = lat1 * math.Pi / 180
	lo1 = lon1 * math.Pi / 180
	la2 = lat2 * math.Pi / 180
	lo2 = lon2 * math.Pi / 180

	r = 6378100

	h := hsin(la2-la1) + math.Cos(la1)*math.Cos(la2)*hsin(lo2-lo1)
	return 2 * r * math.Asin(math.Sqrt(h))
}

func hsin(theta float64) float64 {
	return math.Pow(math.Sin(theta/2), 2)
}
