CREATE TABLE IF NOT EXISTS public.transactions
(
    pol_id character varying COLLATE pg_catalog."default" NOT NULL,
    prover_id character varying COLLATE pg_catalog."default" NOT NULL,
    witness_id character varying COLLATE pg_catalog."default" NOT NULL,
    id character varying COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT transactions_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public.users
(
    id character varying COLLATE pg_catalog."default" NOT NULL,
    name character varying COLLATE pg_catalog."default" NOT NULL,
    pub_id1 character varying COLLATE pg_catalog."default" NOT NULL,
    pub_id2 character varying COLLATE pg_catalog."default" NOT NULL,
    password character varying COLLATE pg_catalog."default" NOT NULL,
    last_update date NOT NULL,
    public_key character varying COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT users_pkey PRIMARY KEY (id)
);