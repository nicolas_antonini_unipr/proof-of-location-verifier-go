# PoL Verifier

This repository contains the code to start a server that implements the Verifier capabilities in the PoL generation protocol. 

## Configuration

The application requires a [postgres](https://www.postgresql.org/) database running.
The migration scripts can be found in the `dbmigration` folder.

The server requires a yaml configuration file, that can be provided through the command-line parameter `config`, with the following parameters:

|                |Description                                                     |Type  |
|----------------|----------------------------------------------------------------|------|
|serverport      |the port of the server to be started                            |int   |
|dsn             |the dsn of the database                                         |string|
|jwt_private_key |the private signing key for JWT tokens                          |string|
|jwt_expiration  |the expiration time of issued tokens, in hours                  |int   |
|algod_address   |the address of the algod process                                |string|
|kmd_address     |the address of the kmd process                                  |string|
|algod_token     |the token of algod                                              |string|
|kmd_token       |the token of kmd                                                |string|
|algo_pvt_key    |the private key of the algorand account, base64 encoded         |string|
|app_id          |the id of the smart contract for PoL storage                    |int   |

The application will start a http server on the specified port.

The documentation of the APIs can be found in the `swagger` folder.
