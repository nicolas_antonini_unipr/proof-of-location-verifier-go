package main

import (
	"context"
	"database/sql"
	"flag"
	"fmt"
	"net/http"
	"os"
	"time"

	dbx "github.com/go-ozzo/ozzo-dbx"
	routing "github.com/go-ozzo/ozzo-routing/v2"
	"github.com/go-ozzo/ozzo-routing/v2/content"
	"github.com/go-ozzo/ozzo-routing/v2/cors"
	_ "github.com/lib/pq"
	"org.nicolasantonini/verifier/internal/accesslog"
	"org.nicolasantonini/verifier/internal/auth"
	"org.nicolasantonini/verifier/internal/checkin"
	"org.nicolasantonini/verifier/internal/config"
	"org.nicolasantonini/verifier/internal/dbcontext"
	"org.nicolasantonini/verifier/internal/errors"
	"org.nicolasantonini/verifier/internal/healthcheck"
	"org.nicolasantonini/verifier/internal/log"
	"org.nicolasantonini/verifier/internal/polreqquest"
	"org.nicolasantonini/verifier/internal/proofoflocation"
	"org.nicolasantonini/verifier/internal/pseudonyms"
	"org.nicolasantonini/verifier/internal/publickey"
	"org.nicolasantonini/verifier/internal/smartcontract"
)

//The version of the server application
var Version = "0.4.1"

//Flags that can be provided from command-line
var cmdFlagsConfig = flag.String("config", "C:\\Users\\Nicolas\\Desktop\\verifier\\config\\local.yaml", "path to the config file")

func main() {
	logger := log.New().With(context.TODO(), "version", Version)

	cfg, err := config.Load(*cmdFlagsConfig, logger)
	if err != nil {
		logger.Errorf("failed to load application configuration: %s", err)
		os.Exit(-1)
	}

	db, err := dbx.MustOpen("postgres", cfg.DSN)
	if err != nil {
		logger.Error(err)
		os.Exit(-1)
	}

	_, err = smartcontract.Setup(cfg.AccountPrivateKey, cfg.AlgodAddress, cfg.AlgodToken, cfg.KmdAddress, cfg.KmdToken, cfg.AppIdx)
	if err != nil {
		logger.Error(err)
		os.Exit(-1)
	}

	db.QueryLogFunc = logDBQuery(logger)
	db.ExecLogFunc = logDBExec(logger)

	defer func() {
		if err := db.Close(); err != nil {
			logger.Error(err)
		}
	}()

	address := fmt.Sprintf(":%v", cfg.ServerPort)

	httpServer := &http.Server{
		Addr:    address,
		Handler: buildHandler(logger, dbcontext.New(db), cfg),
	}

	go routing.GracefulShutdown(httpServer, 10*time.Second, logger.Infof)

	logger.Infof("server %v is running at %v", Version, address)
	if err := httpServer.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		logger.Error(err)
		os.Exit(-1)
	}
}

//Configures the http handler
func buildHandler(logger log.Logger, db *dbcontext.DB, cfg *config.Config) http.Handler {
	router := routing.New()

	router.Use(
		accesslog.Handler(logger),
		errors.Handler(logger),
		content.TypeNegotiator(content.JSON),
		cors.Handler(cors.AllowAll),
	)

	healthcheck.RegisterHandlers(router, Version)

	rg := router.Group("/v1")

	authHandler := auth.Handler(cfg.JWTPrivateKey)

	checkin.RegisterHandlers(rg.Group(""),
		checkin.NewService(checkin.NewRepository(db, logger), logger),
		authHandler,
		logger)

	pseudonyms.RegisterHandlers(rg.Group(""),
		pseudonyms.NewService(pseudonyms.NewRepository(db, logger), logger),
		authHandler,
		logger)

	polreqquest.RegisterHandlers(rg.Group(""),
		polreqquest.NewService(polreqquest.NewRepository(db, logger), logger),
		authHandler,
		logger)

	proofoflocation.RegisterHandlers(rg.Group(""), proofoflocation.NewService(proofoflocation.NewRepository(db, logger), logger, cfg.JWTPrivateKey), authHandler, logger)

	auth.RegisterHandlers(rg.Group(""),
		auth.NewService(cfg.JWTPrivateKey, cfg.JWTExpiration, auth.NewRepository(db, logger), logger),
		logger)

	publickey.RegisterHandlers(rg.Group(""), authHandler, logger)

	return router
}

func logDBQuery(logger log.Logger) dbx.QueryLogFunc {
	return func(ctx context.Context, t time.Duration, sql string, rows *sql.Rows, err error) {
		if err == nil {
			logger.With(ctx, "duration", t.Milliseconds(), "sql", sql).Info("DB query successful")
		} else {
			logger.With(ctx, "sql", sql).Errorf("DB query error: %v", err)
		}
	}
}

func logDBExec(logger log.Logger) dbx.ExecLogFunc {
	return func(ctx context.Context, t time.Duration, sql string, result sql.Result, err error) {
		if err == nil {
			logger.With(ctx, "duration", t.Milliseconds(), "sql", sql).Info("DB execution successful")
		} else {
			logger.With(ctx, "sql", sql).Errorf("DB execution error: %v", err)
		}
	}
}
