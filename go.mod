module org.nicolasantonini/verifier

go 1.16

require github.com/go-ozzo/ozzo-validation/v4 v4.3.0

require go.uber.org/zap v1.13.0

require github.com/google/uuid v1.1.1

require gopkg.in/yaml.v2 v2.4.0

require github.com/go-ozzo/ozzo-dbx v1.5.0

require github.com/go-ozzo/ozzo-routing/v2 v2.3.0

require github.com/dgrijalva/jwt-go v3.2.0+incompatible

require (
	github.com/algorand/go-algorand-sdk v1.12.0
	github.com/lib/pq v1.2.0
)
